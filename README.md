
# Mock LDAP Server

This role will deploy a mock LDAP server for use in developing other roles.

See defaults/main.yml for configurable variables such as port and user list.

The server itself is written in nodejs and deployed from this project:  
https://www.npmjs.com/package/ldap-server-mock
